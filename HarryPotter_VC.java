package HarryPotter_VC;
import robocode.*;
import java.awt.Color;

/****
 * Nome do Robô: HarryPotter_VC
 * Empresa: Solutis Tecnologia
 * Participante: Vanessa C. Correia
 * Quero ser DEV! Talent Sprint Solutis
 ***/

public class  HarryPotter_VC extends Robot
{
	
	public void run() {
		
		setColors(Color.red,Color.black,Color.yellow); // body,gun,radar

		// Loop principal
		while(true) {	
			turnGunRight(90);
			back(150);
		            }
	    }

	/**
	 * onScannedRobot: O que fazer quando o robô detectar o outro
	 */
	public void onScannedRobot(ScannedRobotEvent e) 
	{
        // Atira no oponente detectado
          fire(3);
		  if(getEnergy() < 80.0){
		  double angulo = e.getBearing();
		  turnGunRight(angulo);
		  fire(4);			
		  turnGunRight(90);
		  back(150);
		  } 
	 }

	/**
	 * onHitByBullet: Ao ser ser atigindo:
	 */
	
	public void onHitByBullet(HitByBulletEvent e) 
	{
		double angulo = e.getBearing();
		turnGunRight(angulo);
			back(150);
	}
	
	/**
	 * onHitWall: Ao bater na parede, execute o comando:
	 */
	public void onHitWall(HitWallEvent e) {
	
		turnRight(100); // vire a direita 
			back(150); //  tras
	}
	
	public void onBulletHit(BulletHitEvent e) 	{
		turnRight(100);
	}
	public void onHitRobot(HitRobotEvent e) {
		double angulo = e.getBearing();
		turnGunRight(angulo); //Gire a arma do robô para a direita 
		fire(3);              // atire
		back(50);             //tras
	}
}
